from xl.lyrics import LyricSearchMethod
from xl.lyrics import LyricsNotFoundException
from xl import event
import re
import urllib
import urllib2

def enable(exaile):
    """
        Enable the plugin to fetch lyrics from xiami.com
    """
    if exaile.loading:
        event.add_callback(_enable, "exaile_loaded")
    else:
        _enable(None, exaile, None)

def _enable(eventname, exaile, nothing):
    exaile.lyrics.add_search_method(XiamiLyrics())


def disable(exaile):
    exaile.lyrics.remove_search_method_by_name("xiami")

class XiamiLyrics(LyricSearchMethod):

    name= "xiami"
    display_name = "Xiami"
    
    def find_lyrics(self, track):
        try:
            title = track.get_tag_raw("title")[0].encode('utf-8').strip()
            self.name = title + " - xiami"
            query = urllib.quote_plus(title)
        except TypeError:
            raise LyricsNotFoundException
        search = "http://www.xiami.com/search/song?key=%s" % query
        try:
            ret = urllib2.urlopen(search).read()
        except IOError:
            raise LyricsNotFoundException

        try:
            (lyrics, url) = self.get_lyrics(ret)
        except:
            raise LyricsNotFoundException

        return (lyrics, self.name, url)

    def parse_lyric(self, lyric):
        lyric = re.sub(r'<.+?>', '', lyric)
        lyric = re.sub(r'(?:\r?\n)', '\n', lyric)
        # for HTML escaping
        lyric = re.sub(r'&lt;', '<', lyric)
        lyric = re.sub(r'&gt;', '>', lyric)
        # for overescaping
        lyric = re.sub(r"\\'", "'", lyric)
        lyric = '\n'.join([i.decode('utf-8').strip().encode('utf-8')
                          for i in lyric.split('\n')]).strip()

        return lyric

    def get_lyrics(self, ret):
        url = self.get_song_page(ret)
        page = urllib2.urlopen(url).read()
        match = re.search(r'<div class="lrc_main">(.+?)</div>',
                page, re.M | re.S)
        if not match:
            raise LyricsNotFoundException

        return (self.parse_lyric(match.groups()[0]), url)

    def get_song_page(self, ret):
        match = re.search(r'/song/(\d+)', ret, re.M)
        if not match:
            raise LyricsNotFoundException
        songid = match.groups()[0] # Always get the 1st candidate
        url = "http://www.xiami.com/song/%s" % songid
        return url

